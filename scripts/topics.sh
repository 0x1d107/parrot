#!/bin/bash
SECTIONS=(1950 2200)
mkdir -p topics
cd topics
echo ${SECTIONS[*]}|tr ' ' '\n'|xargs -I '{}' curl --compressed 'http://api.rutracker.org/v1/static/pvc/f/{}' \
|jq -r '.result|keys[]'|pr -t100 -s',' -W 9001|tr -d '\t '\
|sed 's|^|http://api.rutracker.org/v1/get_tor_topic_data?by=topic_id\&val=|'\
| ~/Projects/wario/wario.py -c 16 --rename '{index}.json' -f '-' 
cd ..
cat topics/*.json |jq -r \
    '.result|select (. != null)|.[]|select(.!=null)|([.info_hash,.topic_title])|@csv'>topics.csv
sqlite3 -init topics.sql topics.db ''
rm -r topics topics.csv
